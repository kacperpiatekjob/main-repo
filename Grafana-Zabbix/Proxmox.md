# Proxmox configuration, Zabbix + Grafana setup, Bash Scripts and Ansible playbooks

Screens will be provided after doing Things to do :D

1. Proxmox configuration + installation 

    Proxmox is setup on VMware Workstation
    Virtual Environment 7.3-3
     
    Storage: 220 GB Hardware disk, there are 3 partitions for
    - iso images 20 GB
    - VM storage 100 GB
    - Backups 100 GB

   ![Alt text](./screens/proxmox.png "Proxmox env")

2. Zabbix + Grafana 

    Everything is working correctly.

    Things to do:
    - [ ] Setup trigger on Zabbix to automatically add a server by using config file and metadata


3. Bash Scripts

    Bash scripts are made to back up the MYSQL database using dump, and update/clean env.
    They run by cronjobs once a week.

    Things to do:
    - [ ] Use cloud provider to store dump Backup - S3 storage - dump now has around 2 MB

4. Ansible Playbooks

    Ansible playbooks are written for installing Docker on VMs. Normal set-up on SSH inventory. 

    Things to do:
    - [ ] Check if the service (Docker) on every server is working and if not restart
    - [ ] Os update if everything is correct and if not restart
    - [ ] Deleting one kernel version behind






 



