#!/bin/bash 

# Configuration variables
DB_HOST="localhost"

# X stands only for this repo, on server it's normal 
# (You can also use env variables or Vault but it's only for home-labs env)
DB_USER="X"
DB_PASSWORD="X"
DB_NAME="X"

BACKUP_DIR="/var/backups/mysql"
BACKUP_FILENAME="$BACKUP_DIR/$(date +%Y-%m-%d_%H-%M-%S)_backup.sql"

# Create backup directory if it doesn't exist
mkdir -p $BACKUP_DIR

# Dump database to backup file
mysqldump --host=$DB_HOST --user=$DB_USER --password=$DB_PASSWORD $DB_NAME > $BACKUP_FILENAME

# Check if backup file was created
if [ -f $BACKUP_FILENAME ]; then
            echo "MySQL backup created: $BACKUP_FILENAME"
    else
                echo "Error: MySQL backup failed"
                    exit 1
fi

# Clean up old backups (keep only last 7 days)
find $BACKUP_DIR -type f -mtime +7 -delete