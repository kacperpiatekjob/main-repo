#!/bin/bash

# Checking server Errors
LOGS="/var/log/syslog"

# Directory for logs
SCRIPTS_DIRECTORY="/home/zabbix/scripts/logs"

# Check if syslogs exists
if [ ! -f $LOGS ]; then
        echo "Logs file is invalid"
        # Exit code to leave 
        exit 1
else
        # Creates logs directory if not exists
        mkdir -p $SCRIPTS_DIRECTORY

        # Creating warning.log and checking for warning messages
        grep -i warning $LOGS > "$SCRIPTS_DIRECTORY/warning.log"

        # Creating error.log and checking for error messages
        grep -i error $LOGS > "$SCRIPTS_DIRECTORY/error.log"

        # Creating fail.log anmd checking for fail messages
        grep -i fail $LOGS > "$SCRIPTS_DIRECTORY/fail.log"

fi