#!/bin/bash

# On the server is only one disk, it won't work if there are multiple
DISK_SPACE=$(df -h | awk 'FNR == 4 { print "Disk usage in %: "$5; exit }')

# How much memory is left in %
FREE_MEMORY=$(free -mh | awk 'FNR == 2 { print "Free memory in %: " $4/$2 * 100; exit }' | sed 's/\..*//')

# How long is instance running 
INSTANCE_UPTIME=$(uptime -p | awk '{gsub(/,/,"",$0); print "Running time instance: " $0; exit }' | tr -d '\n')

# Output
echo $DISK_SPACE
echo $FREE_MEMORY
echo $INSTANCE_UPTIME