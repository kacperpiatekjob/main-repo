#!/bin/bash

# Update and then upgrade packages for Ubuntu 22.04
sudo apt-get update && sudo apt-get upgrade -y

# Clean up unused packages and dependencies
sudo apt-get autoremove -y
sudo apt-get autoclean

# Reboot the server if necessary
if [ -f /var/run/reboot-required ]; then
            echo "Rebooting"
            sudo reboot
fi