# Movie App

Used technologies:
- Python 3 / Flask 
- MySQL 
- Kubernetes 
- Docker 
- GitHub
- ArgoCD
- Helm

## How Apps Works

The current setup includes a Kubernetes infrastructure with the application running on minikube and ArgoCD running on a minikube cluster. ArgoCD is connected to the GitHub repository and configured to automatically sync with every change made to the repository.

![ArgoCD](./screens-docs/argo.png)

## Working app

The Flask movie application works in conjunction with a MySQL database to provide users with search, delete, and add movie functionality.

When a user searches for a movie, the Flask application sends a query to the MySQL database to retrieve any movies that match the search criteria. The results are then displayed to the user.
If a user wants to delete a movie, the Flask application sends a request to the MySQL database to delete the corresponding movie entry.
Similarly, when a user wants to add a movie, the Flask application collects the necessary information from the user and sends a request to the MySQL database to add a new movie entry.
Overall, the Flask movie application and MySQL database work together seamlessly to provide users with a smooth and efficient movie management experience.

![](./screens-docs/working.mov)

## Main page

![Mainpage](./screens-docs/main.png)