import os
from flask import Flask, render_template, request, redirect
from flaskext.mysql import MySQL

app = Flask(__name__)

# Mysql database 
mysql = MySQL(app)

app.config['MYSQL_DATABASE_USER'] = os.environ.get('MYSQL_DATABASE_USER')
app.config['MYSQL_DATABASE_PASSWORD'] = os.environ.get('MYSQL_DATABASE_PASSWORD')
app.config['MYSQL_DATABASE_DB'] = os.environ.get('MYSQL_DATABASE_DB')
app.config['MYSQL_DATABASE_HOST'] = os.environ.get('MYSQL_DATABASE_HOST')

mysql.init_app(app)

@app.route('/')
def main():
    return render_template('main-page.html')

@app.route('/signup')
def signup():
    return "Hello"

@app.route('/login')
def login():
    return render_template('login-page.css')

@app.route('/movies')
def movies():
    cursor = mysql.get_db().cursor()
    cursor.execute('SELECT * FROM hello')
    movies = cursor.fetchall()
    cursor.close()
    return render_template("movies.html", movies=movies)

@app.route('/search', methods=['POST'])
def search_movies():
    # Get the search query from the form submission
    query = request.form['query']
    # Query the database for movies that match the search query
    cursor = mysql.get_db().cursor()
    cursor.execute('SELECT * FROM hello WHERE title LIKE %s', ('%' + query + '%',))
    movies = cursor.fetchall()
    return render_template('movies.html', movies=movies)


@app.route('/new-movie', methods=['GET'])
def new_movie():
    return ''

@app.route('/add-movie', methods=['POST'])
def add_movie():
    title = request.form['title']
    genre = request.form['genre']
    director = request.form['director']
    year = request.form['year']

    # connect to MySQL database
    mydb = mysql.connect()
    mycursor = mydb.cursor() 


    # insert new movie into database
    sql = "INSERT INTO hello (Title, Genre, Director, Release_year) VALUES (%s, %s, %s, %s)"
    val = (title, genre, director, year)
    mycursor.execute(sql, val)
    mydb.commit()
    
    return redirect('/movies')

@app.route('/delete-movie/<int:id>', methods=['POST'])
def delete_movie(id):
        # create database connection
        conn = mysql.get_db()
        cursor = conn.cursor()

        # execute SQL query to delete movie
        sql = "DELETE FROM hello WHERE id=%s"
        cursor.execute(sql, (id,))
        conn.commit()

        # close database connection
        cursor.close()
        conn.close()

        # return success message
        return redirect('/movies')


if __name__ == "__main__":
    app.run(debug=True)