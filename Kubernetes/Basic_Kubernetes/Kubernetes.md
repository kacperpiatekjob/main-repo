# Starting from easy Kubernetes YAML files to more advanced topics using networking, and Helm charts.


## Creating first pod

1) Step is to always make new namespace, because when you deploy anything inside it you 
can just delete namespace with pods/services inisde it, also you can setup network policies. 

    ```kubectl create namespace/ns test```

2) To use namespace you can use -n flag 

    ```kubectl -n test get pod```

3) Let's create first pod 

    ```kubectl run busybox --image=busybox --restart=Never --dry-run=client -o yaml > first_pod.yml -- sleep 3600```

4) Lets create/apply first pod, differencte is important because we can only create for the first time using it, and update config we can all the time

    ```kubectl -n test create -f Basic_Kubernetes/first_pod.yml```

5) Let's check what we got!

    ```kubectl -n test get po```


## Creating deployment 

1) Creating first deployment in same namespace ```test```

    ```kubectl -n test create deploy nginx --image nginx:alpine --replicas 2 --dry-run=client -o yaml > first_deploy.yml```

2) Let's create your first deploy 

    ```kubectl create -f first_deploy.yml```

3) Let's check what we got there

    ```kubectl -n test describe deploy```

4) Well, everything sounds right so we can change ```nginx:alpine``` image on something that is invalid to make rollout

    ```kubectl -n test rollout undo deploy```

## Basic networking

1) Expose service, in this example it's ```nginx``` deployment

   ```kubectl -n test expose deploy nginx --port 80```

2) Check if everything works, using 

    ```kubectl -n test get svc```
    ```kubectl -n test get ep```

