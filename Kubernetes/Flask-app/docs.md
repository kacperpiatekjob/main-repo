# Flask app as Docker image running on local minikube

1. The hardest part is to run image on minikube locally.

    You have to use it to even be able to run local image on Kubernetes on minikube.

    ```eval $(minikube docker-env)``` 

2. Create Docker image

    ```docker build -t flask-app:0.1 .```

3. Create pod file and run it

    The most important part is ```--image-pull-policy=Never``` because it will not try to find any image online repository

    ```kubectl run flask --image=flask-app:0.1 --image-pull-policy=Never --dry-run=client -o yaml > test.yml```
    
    ```kubectl create -f test.yml```

4. Port forward

    ```kubectl port-forward flask 5000:5000```

5. Finall screens

    ![Flask app front](./screens/flask-app-front.png "Flask app front")
    ![Working Kubernetes](./screens/kubernetes-forwarding.png "Kubernetes forwarding")
