# Showing difference between blue green startegy and canary deployment using Kubernetes native services

## Blue-green deployment

1. Create namespace

    ```kubectl create ns test```

2.  Create blue and green deployments for different versions

    ```kubectl -n test create deploy nginx-blue --image nginx:alpine --replicas 1 --dry-run=client -o yaml > nginx_blue_deployment.yml```

    ```kubectl -n test create deploy nginx-green --image nginx:alpine --replicas 1 --dry-run=client -o yaml > nginx_green_deployment.yml```

3. Create deployments where files of them are located

    ```kubectl create -f .```

4. Check if they exists

    ```kubectl -n test get deploy```

5. Expose deployment blue using services

    ```kubectl -n test expose deploy nginx-blue --port 80```

6. Check service selector

    ```kubectl -n test get svc -o wide```

As you can see it's saying ```app=nginx-blue```

7. Change selector to serve version green

    ```kubectl -n test get svc -o wide```

8. Verification :), as you can see now everything is served on green deployment

    ```kubectl -n test get ep```

    ```kubectl -n test get po -o wide```

## Canary deployment

1. Delete old namespace and create a new one
2. Create new deployment
    
    ```kubectl -n test create deploy canary --image nginx:alpine --replicas 1 --dry-run=client -o yaml > canary_deployment.yml```

3. Create deployment

    ```kubectl create -f canary_deployment.yml```

4. Expose svc on canary deployment

    ```kubectl -n test expose deploy canary --port 80```

5. Check endpoints on this svc, and you willl see there is only one

    ```kubectl -n test get ep```

6. Change the name in deployment config and create new deployment

    ```kubectl -n test create new_one.yml```

7. Check endpoints and you will see there are 2, it's because it's setup based on pods and there are 1 replica for canary and the other one
for new_one, we can for example split it 7:3 or 9:1 to test new version of application.


