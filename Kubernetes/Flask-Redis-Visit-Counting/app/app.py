from flask import Flask, render_template
from redis import Redis

app = Flask(__name__)
redis = Redis(host='redis.pod.ip', port=6379)

@app.route('/')
def count():
    redis.incr('refreshes')
    count = redis.get('refreshes').decode('utf-8')
    return render_template("index.html", count=count)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
