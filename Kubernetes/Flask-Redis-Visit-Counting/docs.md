# App counting refreshes of page are stored in redis and displayed on flask frontend, everything is working on minikube

## Setup

1. The hardest part is to run image on minikube locally.

    You have to use it to even be able to run image on Kubernetes on minikube.

    ```eval $(minikube docker-env)``` 
2. Run redis pod on minikube to get redis ip

    ```kubectl run redis --image redis```
    ```kubectl get po -o wide```

2. Update app redis host 


3. Run docker build

    ```docker build -t flask-redis .```

4. Create pod file and run it

    ```kubectl run flask --image=flask-redis --image-pull-policy=Never --dry-run=client -o yaml > test.yml```
    ```kubectl create -f test.yml```

5. Port forward

    ```kubectl port-forward flask 5000:5000```


## Video of working counting

![](./screens/record.mov)

## Handling connections

![Working Kubernetes](./screens/screen.png "Kubernetes forwarding")