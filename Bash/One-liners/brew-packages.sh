#! /bin/bash

# If -z means if variable in $1 is null 
if [[ -z $1 ]]; then
	brew list
else
	brew list | grep -i $1
fi
