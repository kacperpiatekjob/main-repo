#! /bin/bash


if [[ -n $1 ]]
then
	PODS=$(kubectl -n $1 get pods | awk 'NR!=1 { print  $3 }')
else
	PODS=$(kubectl get pods | awk 'NR!=1 { print  $3 }')
fi

for pod in $PODS
do
	if [[ $pod != "Running" && $pod != "Completed" ]] 
	then
		# Still improve this part :)
		echo "Check ErrorPodStatus file for more informations"
		echo "Something is not working" > ErrorPodStatus
		exit 1
	fi
done

echo "$PODS" | column -t

echo "Everything is Running or Completed in this namespace"
