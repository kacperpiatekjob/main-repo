#!/bin/bash

FILETOCHECK=$1
DEFAULTFILE="default-file.sh"

if [[ -x $FILETOCHECK && -n $FILETOCHECK ]] 
then
	echo "File exists and have execute permissions"
elif [[ ! -n $FILETOCHECK ]]
then
	echo "New file is created and have execute permissions $USER"
	touch $DEFAULTFILE
	chmod u+x $DEFAULTFILE
else
	echo "Now file have execute permissions"
	chmod u+x $FILETOCHECK
fi

