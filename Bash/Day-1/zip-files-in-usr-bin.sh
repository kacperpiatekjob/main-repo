#! /bin/bash

for file in /{,usr/}bin/zip*
do
	if [[ -x "$file" ]]; then
		echo $file
	fi	
done
