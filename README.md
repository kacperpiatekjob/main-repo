# Purpose

Hello, this repo is for projects based on Cloud stack technologies.

## Bash

You can find here Bash scripts for automation, and just testing purpose.

## Grafana-Zabbix -> home labs

You can expect there home lab env setup on proxmox, Zabbix for metrics, and Grafana for monitoring. 
There is one VM used as a DHCP server, one main machine with an Ubuntu server, and another two Debian 11 for using Ansible playbooks 
and both of them are connected to Zabbix.

## Kubernetes

You can expect there projects based on K8s, Canary/Blue-Green deployments, and container apps on minikube and in Clouds.
Most apps will be made in Python. In future also ArgoCD to get GitOps :D

## Python

You can see there scripts for Linux/Clouds envs.

## Terraform

You can expect there Terraform for AWS Cloud Base
